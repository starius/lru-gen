package examples

//go:generate lru-gen -key int -value string -package int2string -output int2string/lru.go
//go:generate lru-gen -key int -value *string -package int2pstring -output int2pstring/lru.go
//go:generate lru-gen -key string -value *int -package string2pint -output string2pint/lru.go
//go:generate lru-gen -key string -value []byte -package kvwithevict -output kvwithevict/lru.go -with-callback
